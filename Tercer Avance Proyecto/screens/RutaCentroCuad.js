import React, { Component } from "react";
import { Dimensions, StyleSheet, View } from "react-native";
import MapView, { Marker } from "react-native-maps";
import MapViewDirections from "react-native-maps-directions";

const { width, height } = Dimensions.get("window");
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
const GOOGLE_MAPS_APIKEY = "AIzaSyBuAjMS_UH8pCAJKcpxTwkbzg6iItU4xMc";

class RutaCentro extends Component {
  constructor(props) {
    super(props);

    this.state = {
      userLocation: null,
      coordinates: [
        { latitude: 32.53407550614947, longitude: -117.03867603250026 },
        { latitude: 32.53214053874429, longitude: -117.03034763282824 },
        { latitude: 32.516626051762465, longitude: -117.00747857656988 },
        { latitude: 32.51118607489634, longitude: -116.9868142399839 },
        { latitude: 32.495201890846516, longitude: -116.96039325923093 },
        { latitude: 32.47669280741377, longitude: -116.9363099689483 },
        { latitude: 32.45399998188011, longitude: -116.88099210803543 },
      ],
      stopNames: [
        "Inicio, DAX Centro ",
        "Blvd Sánchez Taboada, frente a Costco",
        "Blvd Sánchez Taboada, frente a 260 Bar & Grill",
        "Av. 20 de Noviembre, frente a Mueblería CHIC",
        "Blvd. Gustavo Díaz Ordaz, frente a Carrousel",
        "Av. Lomas Verdes, en contraesquina a Farmacia La Más Barata",
        "Haemonetics",
      ],
    };

    this.mapView = null;
  }

  onReady = (result) => {
    this.mapView.fitToCoordinates(result.coordinates, {
      edgePadding: {
        right: width / 10,
        bottom: height / 10,
        left: width / 10,
        top: height / 10,
      },
    });
  };

  onError = (errorMessage) => {
    console.log(errorMessage);
  };

  render() {
    return (
      <View style={styles.mapContainer}>
        <MapView
          initialRegion={{
            latitude: this.state.userLocation
              ? this.state.userLocation.latitude
              : 32.53407550614947,
            longitude: this.state.userLocation
              ? this.state.userLocation.longitude
              : -117.03867603250026,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA,
          }}
          style={styles.map}
          ref={(c) => (this.mapView = c)}
        >
          {this.state.coordinates.map((coordinate, index) => (
            <Marker
              key={index}
              coordinate={coordinate}
              title={this.state.stopNames[index]}
            />
          ))}
          {this.state.userLocation && (
            <Marker
              coordinate={{
                latitude: this.state.userLocation.latitude,
                longitude: this.state.userLocation.longitude,
              }}
              title="Usted está aquí."
            />
          )}
          <MapViewDirections
            origin={this.state.coordinates[0]}
            destination={
              this.state.coordinates[this.state.coordinates.length - 1]
            }
            waypoints={this.state.coordinates.slice(1, -1)}
            mode="DRIVING"
            apikey={GOOGLE_MAPS_APIKEY}
            language="en"
            strokeWidth={4}
            strokeColor="#5065F6"
            onStart={(params) => {
              console.log(
                `Started routing between "${params.origin}" and "${
                  params.destination
                }"${
                  params.waypoints.length
                    ? " using waypoints: " + params.waypoints.join(", ")
                    : ""
                }`
              );
            }}
            onReady={this.onReady}
            onError={this.onError}
            resetOnChange={false}
          />
        </MapView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  mapContainer: {
    width: 350, // Define el ancho del contenedor del mapa
    height: 400, // Define la altura del contenedor del mapa
    borderRadius: 10, // Opcional: agrega bordes redondeados para mejorar la apariencia
    overflow: "hidden", // Opcional: asegúrate de que el mapa no se desborde del contenedor
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
  container: {
    ...StyleSheet.absoluteFillObject,
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
});

export default RutaCentro;
