import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ScrollView,
  TextInput,
  KeyboardAvoidingView,
  Platform,
  Linking,
} from "react-native";
import { useNavigation } from "@react-navigation/native";
import { FontFamily, Padding, FontSize, Color, Border } from "../GlobalStyles";
import { useTheme } from "../ThemeContext";
import { Image } from "expo-image";

const SoporteTecnico = () => {
  const { isDarkMode } = useTheme();
  const [email, setEmail] = useState("");
  const [telefono, setTelefono] = useState("");
  const [opinion, setOpinion] = useState("");
  const [opinionLength, setOpinionLength] = useState(0);

  const darkModeImageWsp = require("../assets/whatsappBlanc.png");
  const lightModeImageWsp = require("../assets/whatsapp.png");
  const imageSourceWsp = isDarkMode ? darkModeImageWsp : lightModeImageWsp;

  const darkModeImageErr = require("../assets/frame2Blanc.png");
  const lightModeImageErr = require("../assets/frame2.png");
  const imageSourceErr = isDarkMode ? darkModeImageErr : lightModeImageErr;

  const formatTelefono = (input) => {
    let cleaned = ("" + input).replace(/\D/g, "");

    let formatted = "";
    if (cleaned.length > 0) {
      formatted += "(" + cleaned.substring(0, 3);
    }
    if (cleaned.length >= 3) {
      formatted += ") ";
      if (cleaned.length === 3 && input[input.length - 1] !== ")") {
        formatted += cleaned.substring(3);
      } else {
        formatted += cleaned.substring(3, 6);
      }
    }
    if (cleaned.length > 6) {
      formatted += "-" + cleaned.substring(6, 10);
    }
    return formatted;
  };

  const handleChangeTelefono = (input) => {
    setTelefono(formatTelefono(input));
  };

  const handleChangeOpinion = (input) => {
    setOpinion(input);
    setOpinionLength(input.length);
  };

  const navigation = useNavigation();
  const irAtras = () => {
    navigation.goBack();
  };
  const irErrores = () => {
    navigation.navigate("ReporteDeErrores");
  };
  const handleOpenWhatsApp = async () => {
    await Linking.openURL("https://wa.me/+526646945260");
  };

  const handleEnviarCorreo = () => {
    const subject = "Opinión sobre la aplicación";
    const body = `Correo electrónico: ${email}\nTeléfono: ${telefono}\nOpinión: ${opinion}`;
    const destinatario = "axelrojero19@gmail.com"; // Dirección de correo electrónico del destinatario
    const mailtoUrl = `mailto:${destinatario}?subject=${encodeURIComponent(
      subject
    )}&body=${encodeURIComponent(body)}`;
    Linking.openURL(mailtoUrl);
    navigation.navigate("Principal");
  };
  const darkModeArrow = require("../assets/icleftBlanc.png");
  const lightModeArrow = require("../assets/icleft.png");

  const imageSource = isDarkMode ? darkModeArrow : lightModeArrow;

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === "ios" ? "padding" : "height"}
      style={styles.container}
    >
      <ScrollView style={isDarkMode ? styles.darkBackground : styles.safeArea}>
        <View style={styles.soporteTecnico}>
          <View style={styles.topBar}>
            <View style={[styles.content, styles.rowFlexBox]}>
              <TouchableOpacity onPress={irAtras}>
                <Image
                  style={styles.icLeftIcon}
                  contentFit="cover"
                  source={imageSource}
                />
              </TouchableOpacity>
              <Text
                style={[
                  styles.titleBac,
                  styles.titleTypoBac,
                  isDarkMode ? styles.darkText : styles.lightText,
                ]}
              >
                Soporte técnico
              </Text>
            </View>
          </View>
          <View style={[styles.sectionTitle, styles.itemFlexBox]}>
            <View style={styles.text}>
              <Text
                style={[
                  styles.title1,
                  styles.titleTypo,
                  isDarkMode ? styles.darkText : styles.lightText,
                ]}
              >
                Reportar errores
              </Text>
            </View>
          </View>
          <View style={styles.list}>
            <TouchableOpacity onPress={irErrores} style={styles.item}>
              <Image
                style={styles.frameIcon}
                contentFit="cover"
                source={imageSourceErr}
              />
              <View style={styles.textContainer}>
                <Text
                  style={[
                    isDarkMode ? styles.darkText : styles.lightText,
                    styles.featureLabel,
                  ]}
                >
                  Reportar error
                </Text>
                <Text
                  style={[
                    styles.description,
                    isDarkMode ? styles.darkText : styles.lightText,
                  ]}
                >
                  Envía un reporte para corregir un error en la aplicación
                </Text>
              </View>
              <Image
                style={styles.itemChild}
                source={require("../assets/vector-2001.png")}
              />
            </TouchableOpacity>
            <TouchableOpacity onPress={handleOpenWhatsApp} style={styles.item}>
              <Image
                style={styles.frameIcon}
                contentFit="cover"
                source={imageSourceWsp}
              />
              <View style={styles.textContainer}>
                <Text
                  style={[
                    isDarkMode ? styles.darkText : styles.lightText,
                    styles.featureLabel,
                  ]}
                >
                  Whatsapp
                </Text>
                <Text
                  style={[
                    styles.description,
                    isDarkMode ? styles.darkText : styles.lightText,
                  ]}
                >
                  Comunícate con nosotros vía WhatsApp
                </Text>
              </View>
              <Image
                style={styles.itemChild}
                source={require("../assets/vector-2001.png")}
              />
            </TouchableOpacity>
          </View>

          <View style={styles.sectionTitle}>
            <View style={styles.text}>
              <Text
                style={[
                  styles.title1,
                  styles.titleTypo,
                  isDarkMode ? styles.darkText : styles.lightText,
                ]}
              >
                Información de contacto
              </Text>
            </View>
          </View>
          <View style={[styles.input, styles.inputSpaceBlock]}>
            <Text
              style={[
                styles.title2,
                styles.text1Typo,
                isDarkMode ? styles.darkText : styles.lightText,
              ]}
            >
              Email
            </Text>
            <View style={[styles.textfield, styles.textfieldBorder]}>
              <TextInput
                style={[
                  styles.text1,
                  styles.text1Typo,
                  isDarkMode ? styles.darkText : styles.lightText,
                ]}
                value={email}
                onChangeText={setEmail}
                placeholder="correo@example.com"
                placeholderTextColor={
                  isDarkMode ? styles.darkText.color : styles.lightText.color
                }
                keyboardType="email-address"
              />
            </View>
          </View>
          <View style={[styles.input, styles.inputSpaceBlock]}>
            <Text
              style={[
                styles.title2,
                styles.text1Typo,
                isDarkMode ? styles.darkText : styles.lightText,
              ]}
            >
              Teléfono
            </Text>
            <View style={[styles.textfield, styles.textfieldBorder]}>
              <TextInput
                style={[styles.text1, styles.text1Typo]}
                value={telefono}
                onChangeText={handleChangeTelefono}
                placeholder="(664) 123-4567"
                placeholderTextColor={
                  isDarkMode ? styles.darkText.color : styles.lightText.color
                }
                keyboardType="numeric"
              />
            </View>
          </View>
          <View style={[styles.input, styles.inputSpaceBlock]}>
            <Text
              style={[
                styles.title1,
                styles.titleTypo,
                isDarkMode ? styles.darkText : styles.lightText,
              ]}
            >
              Opinion
            </Text>
            <View style={[styles.textfield, styles.textfieldBorder]}>
              <TextInput
                style={[
                  isDarkMode ? styles.darkText : styles.lightText,
                  styles.text1,
                  styles.text1Typo,
                  {
                    minHeight: Math.max(
                      120,
                      20 + 12 * opinion.split("\n").length
                    ),
                  },
                ]}
                placeholder="Escribe tu opinión aquí"
                placeholderTextColor={
                  isDarkMode ? styles.darkText.color : styles.lightText.color
                }
                value={opinion}
                onChangeText={handleChangeOpinion}
                maxLength={200}
                multiline
              />
            </View>
            <Text
              style={[
                styles.info,
                {
                  color: isDarkMode
                    ? opinionLength >= 180
                      ? "red"
                      : "lightgray"
                    : opinionLength >= 180
                    ? "red"
                    : "black",
                },
              ]}
            >
              Caracteres restantes: {200 - opinionLength}
            </Text>
          </View>
          <TouchableOpacity
            style={[styles.button, styles.inputSpaceBlock]}
            onPress={handleEnviarCorreo}
          >
            <View style={styles.primary}>
              <Text style={[styles.title6, styles.titleTypo]}>
                Enviar opinión
              </Text>
            </View>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </KeyboardAvoidingView>
  );
};

const styles = StyleSheet.create({
  description: {
    fontSize: FontSize.size_xs,
    fontFamily: FontFamily.robotoRegular,
  },
  featureLabel: {
    fontSize: FontSize.size_sm,
    fontFamily: FontFamily.robotoRegular,
  },
  textContainer: {
    marginLeft: 8,
  },
  icon: {
    fontSize: FontSize.size_xl,
    lineHeight: 32,
    textAlign: "center",
    fontFamily: FontFamily.robotoRegular,
  },
  iconContainer: {
    width: 32,
    height: 32,
    borderRadius: Border.br_base,
    justifyContent: "center",
    alignItems: "center",
  },
  item: {
    flexDirection: "row",
    alignItems: "center",
    paddingHorizontal: 0,
    paddingVertical: Padding.p_xs,
  },
  list: {
    marginTop: 12,
    paddingHorizontal: Padding.p_xs,
  },
  darkBackground: {
    flex: 1,
    backgroundColor: Color.colorDiscord,
  },
  darkText: {
    color: "#fff", // Texto blanco para modo oscuro
  },
  lightText: {
    color: "#000", // Texto negro para modo claro
  },
  rowFlexBox: {
    flexDirection: "row",
    alignSelf: "stretch",
  },
  container: {
    flex: 1,
  },
  topBar: {
    alignSelf: "stretch",
  },
  icLeftIcon: {
    width: 24,
    height: 24,
  },
  itemFlexBox: {
    flexDirection: "row",
    alignItems: "center",
  },
  safeArea: {
    flex: 1,
    backgroundColor: Color.colorWhite,
  },
  titleTypo: {
    textAlign: "left",
    fontFamily: FontFamily.robotoMedium,
    fontWeight: "500",
  },
  titleTypoBac: {
    fontFamily: FontFamily.robotoMedium,
    fontWeight: "500",
    lineHeight: 24,
    textAlign: "left",
  },
  inputSpaceBlock: {
    paddingVertical: 0,
    marginTop: 12,
    paddingHorizontal: Padding.p_xs,
    overflow: "hidden",
    alignSelf: "stretch",
  },
  text1Typo: {
    lineHeight: 20,
    fontSize: FontSize.size_sm,
    textAlign: "left",
  },
  textfieldBorder: {
    marginTop: 4,
    borderWidth: 1,
    borderColor: Color.colorGray_200,
    borderStyle: "solid",
    borderRadius: Border.br_7xs,
    alignSelf: "stretch",
    alignItems: "center",
  },
  title: {
    fontSize: FontSize.size_lg,
    lineHeight: 24,
    fontWeight: "500",
    fontFamily: FontFamily.robotoMedium,
    textAlign: "left",
    color: Color.colorBlack,
    alignSelf: "stretch",
  },
  titleBac: {
    fontSize: FontSize.size_xl,
    marginLeft: 8,
    textAlign: "left",
    flex: 1,
  },
  content: {
    paddingHorizontal: Padding.p_5xs,
    paddingVertical: Padding.p_xs,
    alignItems: "center",
  },
  frameIcon: {
    borderRadius: Border.br_base,
    width: 32,
    height: 32,
    zIndex: 0,
  },
  title1: {
    fontSize: FontSize.size_lg,
    lineHeight: 24,
    textAlign: "left",
    alignSelf: "stretch",
  },
  text: {
    flex: 1,
  },
  sectionTitle: {
    marginTop: 12,
    paddingHorizontal: Padding.p_xs,
    flexDirection: "row",
    alignSelf: "stretch",
    alignItems: "center",
  },
  title2: {
    fontFamily: FontFamily.robotoMedium,
    fontWeight: "500",
    fontSize: FontSize.size_sm,
    alignSelf: "stretch",
  },
  text1: {
    fontFamily: FontFamily.robotoRegular,
    height: 20,
    overflow: "hidden",
    flex: 1,
    verticalAlign: "top",
  },
  textfield: {
    paddingVertical: Padding.p_5xs,
    paddingHorizontal: Padding.p_xs,
    flexDirection: "row",
  },
  input: {
    justifyContent: "center",
  },
  title6: {
    fontSize: FontSize.size_base,
    lineHeight: 22,
    color: Color.colorWhite,
  },
  primary: {
    borderRadius: Border.br_5xs,
    backgroundColor: Color.colorBlack,
    paddingVertical: Padding.p_3xs,
    justifyContent: "center",
    paddingHorizontal: Padding.p_xs,
    alignItems: "center",
    flex: 1,
  },
  button: {
    flexDirection: "row",
  },
  image: {
    backgroundColor: Color.colorGray_300,
    height: 163,
    borderRadius: Border.br_7xs,
    marginTop: 12,
    alignSelf: "stretch",
  },
  soporteTecnico: {
    flex: 1,
    paddingBottom: Padding.p_xs,
    paddingHorizontal: Padding.p_xs,
    alignItems: "flex",
  },
});

export default SoporteTecnico;
