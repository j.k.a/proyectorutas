import React from "react";
import {
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
} from "react-native";
import { FontSize, FontFamily, Color, Padding } from "../GlobalStyles";
import { useNavigation } from "@react-navigation/native";
import { useTheme } from "../ThemeContext";

const AcercaDeNosotros = () => {
  const navigation = useNavigation();
  const { isDarkMode } = useTheme();

  // Navegar hacia atrás
  const irAtras = () => {
    navigation.goBack();
  };
  const darkModeArrow = require("../assets/icleftBlanc.png");
  const lightModeArrow = require("../assets/icleft.png");

  const imageSource = isDarkMode ? darkModeArrow : lightModeArrow;
  return (
    <SafeAreaView style={isDarkMode ? styles.darkBackground : styles.safeArea}>
      <View style={styles.acercaDeNosotros}>
        <View style={styles.content}>
          <TouchableOpacity onPress={irAtras}>
            <Image
              style={styles.icLeftIcon}
              contentFit="cover"
              source={imageSource}
            />
          </TouchableOpacity>
          <Text
            style={[
              styles.title,
              isDarkMode ? styles.darkText : styles.lightText,
            ]}
          >
            Acerca de nosotros
          </Text>
        </View>
        <View style={styles.list}>
          {/* Misión */}
          <View style={styles.article}>
            <View style={[styles.imageContainer, styles.imageLayout]}>
              <Image
                style={styles.imageLayout}
                source={require("../assets/mision-1.png")}
              />
            </View>
            <View style={styles.textContainer}>
              <Text
                style={[
                  styles.titleText,
                  isDarkMode ? styles.darkText : styles.lightText,
                ]}
              >
                Misión
              </Text>
              <Text
                style={[
                  styles.descriptionText,
                  isDarkMode ? styles.darkText : styles.lightText,
                ]}
              >
                Brindar un servicio de transporte de personal eficiente que
                cumpla las expectativas de los usuarios.
              </Text>
            </View>
          </View>

          {/* Visión */}
          <View style={[styles.article, styles.visionMargin]}>
            <View style={[styles.imageContainer, styles.imageLayout]}>
              <Image
                style={styles.imageLayout}
                source={require("../assets/vision-1.png")}
              />
            </View>
            <View style={styles.textContainer}>
              <Text
                style={[
                  styles.titleText,
                  isDarkMode ? styles.darkText : styles.lightText,
                ]}
              >
                Visión
              </Text>
              <Text
                style={[
                  (styles.descriptionText,
                  isDarkMode ? styles.darkText : styles.lightText),
                ]}
              >
                Compromiso de brindar la más alta eficiencia en seguridad,
                puntualidad, amabilidad y confort.
              </Text>
            </View>
          </View>

          {/* Valores */}
          <View style={[styles.article, styles.valoresMargin]}>
            <View style={[styles.imageContainer, styles.imageLayout]}>
              <Image
                style={styles.imageLayout}
                source={require("../assets/valores-1.png")}
              />
            </View>
            <View style={styles.textContainer}>
              <Text
                style={[
                  styles.titleText,
                  isDarkMode ? styles.darkText : styles.lightText,
                ]}
              >
                Valores
              </Text>
              <Text
                style={[
                  styles.descriptionText,
                  isDarkMode ? styles.darkText : styles.lightText,
                ]}
              >
                Ética{"\n"}
                Respeto{"\n"}
                Profesionalismo{"\n"}
                Calidad{"\n"}
                Honestidad
              </Text>
            </View>
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: Color.colorWhite,
  },
  darkBackground: {
    flex: 1,
    backgroundColor: Color.colorDiscord,
  },
  darkText: {
    color: "#fff",
  },
  lightText: {
    color: "#000",
  },
  icLeftIcon: {
    width: 24,
    height: 24,
  },
  title: {
    fontSize: FontSize.size_xl,
    lineHeight: 24,
    fontWeight: "500",
    fontFamily: FontFamily.robotoMedium,
    color: Color.colorBlack,
    textAlign: "left",
    marginLeft: 8,
    flex: 1,
  },
  content: {
    flexDirection: "row",
    paddingHorizontal: Padding.p_5xs,
    paddingVertical: Padding.p_xs,
    alignSelf: "stretch",
    alignItems: "center",
  },
  list: {
    justifyContent: "center",
    paddingHorizontal: Padding.p_xs,
    alignItems: "center",
  },
  article: {
    flexDirection: "row",
    paddingVertical: Padding.p_5xs,
    alignSelf: "stretch",
    marginTop: 12,
  },
  imageLayout: {
    height: 80,
    width: 80,
  },
  imageContainer: {
    flexDirection: "row",
    overflow: "hidden",
  },
  textContainer: {
    flex: 1,
    marginLeft: 12,
  },
  titleText: {
    fontSize: FontSize.size_base,
    fontWeight: "500",
    fontFamily: FontFamily.robotoMedium,
    color: Color.colorBlack,
    textAlign: "left",
    lineHeight: 20,
  },
  descriptionText: {
    fontSize: FontSize.size_xs,
    fontFamily: FontFamily.robotoRegular,
    color: Color.colorBlack,
    textAlign: "left",
    lineHeight: 20,
  },
  visionMargin: {
    marginTop: 8,
  },
  valoresMargin: {
    marginTop: 8,
  },
});

export default AcercaDeNosotros;
