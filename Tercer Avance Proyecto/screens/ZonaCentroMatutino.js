import * as React from "react";
import { Image } from "expo-image";
import { useNavigation } from "@react-navigation/native";

import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  SafeAreaView,
} from "react-native";
import { useTheme } from "../ThemeContext";
import ScheduleContainer from "../components/ScheduleContainer";
import { Color, FontFamily, FontSize, Padding, Border } from "../GlobalStyles";

const ZonaCentroMatutino = () => {
  const { isDarkMode } = useTheme();
  const navigation = useNavigation();
  const irAtras = () => {
    navigation.goBack();
  };
  const darkModeArrow = require("../assets/icleftBlanc.png");
  const lightModeArrow = require("../assets/icleft.png");

  const imageSource = isDarkMode ? darkModeArrow : lightModeArrow;

  return (
    <SafeAreaView style={isDarkMode ? styles.darkBackground : styles.safeArea}>
      <View style={styles.zonaCentroMatutino}>
        <View style={styles.topBar}>
          <View style={[styles.content, styles.contentFlexBox]}>
            <TouchableOpacity onPress={irAtras}>
              <Image
                style={styles.icLeftIcon}
                contentFit="cover"
                source={imageSource}
              />
            </TouchableOpacity>
            <Text
              style={[
                isDarkMode ? styles.darkText : styles.lightText,
                styles.title,
                styles.titleTypo,
              ]}
            >
              Horarios de parada
            </Text>
          </View>
        </View>
        <View style={[styles.sectionTitle, styles.imageSpaceBlock]}>
          <View style={styles.text}>
            <Text
              style={[
                isDarkMode ? styles.darkText : styles.lightText,
                styles.title1,
                styles.titleTypo,
              ]}
            >
              Zona Centro (Matutino)
            </Text>
          </View>
        </View>
        <ScheduleContainer
          estimatedTime="Hora estimada: 5:30 AM"
          estimatedTimeLabel="Hora estimada: 5:40 AM"
          estimatedTimeDisplay="Hora estimada: 5:55 AM"
          estimatedTimeFormatted="Hora estimada: 6:05 AM"
          estimatedTimeText="Hora estimada: 6:20 AM"
          estimatedTimeDescription="Hora estimada: 6:35 AM"
          estimatedTimeLabelText="Hora estimada: 6:50 AM"
        />
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  darkBackground: {
    flex: 1,
    backgroundColor: Color.colorDiscord,
  },
  darkText: {
    color: "#fff", // Texto blanco para modo oscuro
  },
  lightText: {
    color: "#000", // Texto negro para modo claro
  },
  safeArea: {
    flex: 1,
    backgroundColor: Color.colorWhite,
  },
  contentFlexBox: {
    flexDirection: "row",
    alignItems: "center",
  },
  titleTypo: {
    textAlign: "left",
    fontFamily: FontFamily.robotoMedium,
    fontWeight: "500",
    lineHeight: 24,
  },
  imageSpaceBlock: {
    marginTop: 12,
    alignSelf: "stretch",
  },
  topIcon: {
    maxWidth: "100%",
    overflow: "hidden",
    height: 24,
    alignSelf: "stretch",
    width: "100%",
  },
  icLeftIcon: {
    width: 24,
    height: 24,
  },
  title: {
    fontSize: FontSize.size_xl,
    marginLeft: 8,
    flex: 1,
  },
  content: {
    paddingHorizontal: Padding.p_5xs,
    paddingVertical: Padding.p_xs,
    alignSelf: "stretch",
  },
  topBar: {
    alignSelf: "stretch",
  },
  title1: {
    fontSize: FontSize.size_lg,
    alignSelf: "stretch",
  },
  text: {
    flex: 1,
  },
  sectionTitle: {
    paddingHorizontal: Padding.p_xs,
    paddingTop: Padding.p_base,
    flexDirection: "row",
    alignItems: "center",
  },
  image: {
    borderRadius: Border.br_7xs,
    backgroundColor: Color.colorGray_300,
    height: 164,
  },
  zonaCentroMatutino: {
    flex: 1,
    paddingBottom: Padding.p_xs,
    paddingHorizontal: Padding.p_xs,
    alignItems: "flex",
  },
});

export default ZonaCentroMatutino;
