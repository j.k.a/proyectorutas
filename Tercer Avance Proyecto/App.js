const Stack = createNativeStackNavigator();
import * as React from "react";

import { StatusBar } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { useFonts } from "expo-font";
import { ThemeProvider } from "./ThemeContext";
import Principal from "./screens/Principal";
import InicioDeSesion from "./screens/InicioDeSesion";
import ReporteDeErrores from "./screens/ReporteDeErrores";
import Rutas from "./screens/Rutas";
import ZonaCentroMatutino from "./screens/ZonaCentroMatutino";
import ZonaCentroVespertino from "./screens/ZonaCentroVespertino";
import ZonaCentroNocturno from "./screens/ZonaCentroNocturno";
import Perfil from "./screens/Perfil";
import NaturaMatutino from "./screens/NaturaMatutino";
import NaturaVespertino from "./screens/NaturaVespertino";
import NaturaNocturno from "./screens/NaturaNocturno";
import VillaFontanaMatutino from "./screens/VillaFontanaMatutino";
import VillaFontanaVespertino from "./screens/VillaFontanaVespertino";
import VillaFontanaNocturno from "./screens/VillaFontanaNocturno";
import CreacionDeRutas from "./screens/CreacionDeRutas";
import RegistroDeConductor from "./screens/RegistroDeConductor";
import RegistroDeEmpleado from "./screens/RegistroDeEmpleado";
import Ajustes from "./screens/Ajustes";
import MenuAdministradores from "./screens/MenuAdministradores";
import RegistroDeTransporte from "./screens/RegistroDeTransporte";
import Menu from "./screens/Menu";
import Proximamente from "./screens/Proximamente";
import SoporteTecnico from "./screens/SoporteTecnico";
import AcercaDeNosotros from "./screens/AcercaDeNosotros";
import ModoOscuro from "./screens/ModoOscuro";
import BotonesTemporales from "./screens/BotonesTemporales";
import RutaCentro from "./screens/RutaCentro";
import RutaVillaFontana from "./screens/RutaVillaFontana";
import RutaNatura from "./screens/RutaNatura";
import RutaCentroCuad from "./screens/RutaCentroCuad";
import RutaVillaFontanaCuad from "./screens/RutaVillaFontanaCuad";
import RutaNaturaCuad from "./screens/RutaNaturaCuad";

import { createNativeStackNavigator } from "@react-navigation/native-stack";

const App = () => {
  const [hideSplashScreen, setHideSplashScreen] = React.useState(true);

  const [fontsLoaded, error] = useFonts({
    "Roboto-Regular": require("./assets/fonts/Roboto-Regular.ttf"),
    "Roboto-Medium": require("./assets/fonts/Roboto-Medium.ttf"),
  });

  if (!fontsLoaded && !error) {
    return null;
  }

  return (
    <ThemeProvider>
      <StatusBar barStyle="light-content" backgroundColor="#000" />

      <NavigationContainer>
        {hideSplashScreen ? (
          <Stack.Navigator
            screenOptions={{
              headerShown: false,
              animation: "slide_from_right",
            }}
          >
            <Stack.Screen
              name="InicioDeSesion"
              component={InicioDeSesion}
              options={{ headerShown: false }}
            />
            <Stack.Screen
              name="Principal"
              component={Principal}
              options={{ headerShown: false }}
            />
            <Stack.Screen
              name="Proximamente"
              component={Proximamente}
              options={{ headerShown: false }}
            />
            <Stack.Screen
              name="ReporteDeErrores"
              component={ReporteDeErrores}
              options={{ headerShown: false }}
            />
            <Stack.Screen
              name="Rutas"
              component={Rutas}
              options={{ headerShown: false }}
            />
            <Stack.Screen
              name="ZonaCentroMatutino"
              component={ZonaCentroMatutino}
              options={{ headerShown: false }}
            />
            <Stack.Screen
              name="ZonaCentroVespertino"
              component={ZonaCentroVespertino}
              options={{ headerShown: false }}
            />
            <Stack.Screen
              name="ZonaCentroNocturno"
              component={ZonaCentroNocturno}
              options={{ headerShown: false }}
            />
            <Stack.Screen
              name="Perfil"
              component={Perfil}
              options={{ headerShown: false }}
            />
            <Stack.Screen
              name="NaturaMatutino"
              component={NaturaMatutino}
              options={{ headerShown: false }}
            />
            <Stack.Screen
              name="NaturaVespertino"
              component={NaturaVespertino}
              options={{ headerShown: false }}
            />
            <Stack.Screen
              name="NaturaNocturno"
              component={NaturaNocturno}
              options={{ headerShown: false }}
            />
            <Stack.Screen
              name="VillaFontanaMatutino"
              component={VillaFontanaMatutino}
              options={{ headerShown: false }}
            />
            <Stack.Screen
              name="VillaFontanaVespertino"
              component={VillaFontanaVespertino}
              options={{ headerShown: false }}
            />
            <Stack.Screen
              name="VillaFontanaNocturno"
              component={VillaFontanaNocturno}
              options={{ headerShown: false }}
            />
            <Stack.Screen
              name="CreacionDeRutas"
              component={CreacionDeRutas}
              options={{ headerShown: false }}
            />
            <Stack.Screen
              name="RegistroDeConductor"
              component={RegistroDeConductor}
              options={{ headerShown: false }}
            />
            <Stack.Screen
              name="RegistroDeEmpleado"
              component={RegistroDeEmpleado}
              options={{ headerShown: false }}
            />
            <Stack.Screen
              name="Ajustes"
              component={Ajustes}
              options={{ headerShown: false }}
            />
            <Stack.Screen
              name="MenuAdministradores"
              component={MenuAdministradores}
              options={{ headerShown: false }}
            />
            <Stack.Screen
              name="RegistroDeTransporte"
              component={RegistroDeTransporte}
              options={{ headerShown: false }}
            />
            <Stack.Screen
              name="Menu"
              component={Menu}
              options={{ headerShown: false }}
            />
            <Stack.Screen
              name="SoporteTecnico"
              component={SoporteTecnico}
              options={{ headerShown: false }}
            />
            <Stack.Screen
              name="AcercaDeNosotros"
              component={AcercaDeNosotros}
              options={{ headerShown: false }}
            />
            <Stack.Screen
              name="BotonesTemporales"
              component={BotonesTemporales}
              options={{ headerShown: false }}
            />
            <Stack.Screen
              name="RutaCentro"
              component={RutaCentro}
              options={{ headerShown: false }}
            />
            <Stack.Screen
              name="RutaVillaFontana"
              component={RutaVillaFontana}
              options={{ headerShown: false }}
            />
            <Stack.Screen
              name="RutaNatura"
              component={RutaNatura}
              options={{ headerShown: false }}
            />
            <Stack.Screen
              name="RutaCentroCuad"
              component={RutaCentroCuad}
              options={{ headerShown: false }}
            />
            <Stack.Screen
              name="RutaVillaFontanaCuad"
              component={RutaVillaFontanaCuad}
              options={{ headerShown: false }}
            />
            <Stack.Screen
              name="RutaNaturaCuad"
              component={RutaNaturaCuad}
              options={{ headerShown: false }}
            />
            <Stack.Screen
              name="ModoOscuro"
              component={ModoOscuro}
              options={{ headerShown: false }}
            />
          </Stack.Navigator>
        ) : null}
      </NavigationContainer>
    </ThemeProvider>
  );
};
export default App;
