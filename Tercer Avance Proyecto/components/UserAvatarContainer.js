import React, { useMemo, useState } from "react";
import { Image } from "expo-image";

import { StyleSheet, Text, View, ImageSourcePropType } from "react-native";
import { Border, FontSize, FontFamily, Color, Padding } from "../GlobalStyles";
import { TouchableOpacity } from "react-native-gesture-handler";

const getStyleValue = (key, value) => {
  if (value === undefined) return;
  return { [key]: value === "unset" ? undefined : value };
};

const UserAvatarContainer = ({ itemCode, propMarginTop, selectedImage }) => {
  const [imageSource, setImageSource] = useState(itemCode); // Estado para la fuente de la imagen

  // Actualiza la fuente de la imagen cuando se selecciona una nueva imagen
  useMemo(() => {
    if (selectedImage) {
      setImageSource(selectedImage);
    }
  }, [selectedImage]);

  const avatarStyle = useMemo(() => {
    return {
      ...getStyleValue("marginTop", propMarginTop),
    };
  }, [propMarginTop]);

  return (
    <View style={[styles.avatar, avatarStyle]}>
      <View style={styles.titleWrapper}>
        <Text style={styles.title} numberOfLines={1}>
          Jordan Vidaña
        </Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  avatarIcon: {
    borderRadius: Border.br_21xl,
    width: 40,
    height: 40,
    overflow: "hidden",
  },
  title: {
    fontSize: FontSize.size_base,
    lineHeight: 24,
    fontWeight: "500",
    fontFamily: FontFamily.robotoMedium,
    color: Color.colorBlack,
    textAlign: "left",
    height: 24,
    overflow: "hidden",
    alignSelf: "stretch",
  },
  avatar: {
    flexDirection: "row",
    alignItems: "flex-start",
    paddingHorizontal: Padding.p_xs,
    alignSelf: "auto",
  },
});

export default UserAvatarContainer;
