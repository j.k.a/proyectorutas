from django.urls import include, path
from rest_framework import routers
from rest_framework.documentation import include_docs_urls
from . import views
from tasks import views
from rest_framework.routers import DefaultRouter



router = routers.DefaultRouter()

router.register('administrador', views.AdministradorViewSet, basename='administrador')
router.register('conductores', views.ConductoresViewSet, basename='conductores')
router.register('conductorestransp', views.ConductoresTranspViewSet, basename='conductorestransp')
router.register('conductoresturnos', views.ConductoresTurnosViewSet, basename='conductoreturnos')
router.register('disponibilidad', views.DisponibilidadViewSet, basename='disponibilidad')
router.register('empleados', views.EmpleadosViewSet, basename='empleados')
router.register('empleadostransp', views.EmpleadosTranspViewSet, basename='empleadostransp')
router.register('marca', views.MarcaViewSet, basename='marca')
router.register('modelo', views.ModeloViewSet, basename='modelo')
router.register('paradas', views.ParadasViewSet, basename='paradas')
router.register('rutas', views.RutasViewSet, basename='rutas')
router.register('transportes', views.TransportesViewSet, basename='transportes')
router.register('turnos', views.TurnosViewSet, basename='turnos')
router.register('turnosparadas', views.TurnosParadasViewSet, basename='turnosparadas')

urlpatterns = [
    path("api/v1/", include(router.urls)),
    path('docs/', include_docs_urls(title='Tasks API')),
    path('api/filtrar-rutas/', views.FiltrarRutasAPIView.as_view(), name='filtrar-rutas'),
    path('api/filtrar-paradas/', views.FiltrarParadasAPIView.as_view(), name='filtrar-paradas'),
]