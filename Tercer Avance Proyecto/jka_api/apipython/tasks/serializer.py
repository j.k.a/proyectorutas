from rest_framework import serializers

from .models import *

#Modelo para de los models convertirlos en serializar, que son pues se encargan de pasarse a base del framework serializar, a json
# class tablaSerializer():
#     class Meta:
#         model = 'aquielnombredelmodelo'
#         fields = '__all__'


class AdministradorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Administrador
        fields = '__all__'

class ConductoresSerializer(serializers.ModelSerializer):
    class Meta:
        model = Conductores
        fields = '__all__'

class ConductoresTranspSerializer(serializers.ModelSerializer):
    class Meta:
        model = ConductoresTransp
        fields = '__all__'

class ConductoresTurnosSerializer(serializers.ModelSerializer):
    class Meta:
        model = ConductoresTurnos
        fields = '__all__'

class DisponibilidadSerializer(serializers.ModelSerializer):
    class Meta:
        model = Disponibilidad
        fields = '__all__'

class EmpleadosSerializer(serializers.ModelSerializer):
    class Meta:
        model = Empleados
        fields = '__all__'

class EmpleadosTranspSerializer(serializers.ModelSerializer):
    class Meta:
        model = EmpleadosTransp
        fields = '__all__'

class MarcaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Marca
        fields = '__all__'

class ModeloSerializer(serializers.ModelSerializer):
    class Meta:
        model = Modelo
        fields = '__all__'

class ParadasSerializer(serializers.ModelSerializer):
    class Meta:
        model = Paradas
        fields = '__all__'

class RutasSerializer(serializers.ModelSerializer):
    class Meta:
        model = Rutas
        fields = '__all__'

class TransportesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Transportes
        fields = '__all__'

class TurnosSerializer(serializers.ModelSerializer):
    class Meta:
        model = Turnos
        fields = '__all__'

class TurnosParadasSerializer(serializers.ModelSerializer):
    class Meta:
        model = TurnosParadas
        fields = '__all__'

